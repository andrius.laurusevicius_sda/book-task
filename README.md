## Books Rest API task

Live version is available at - [dematic-books.herokuapp.com](https://dematic-books.herokuapp.com/)

### How to run application

- To run application you need JDK 11 version.
- Open project with the selected IDE and run main class `BooksApplication.java` Or you can start the app from root folder using console. Type command `./gradlew bootRun` in console.
- Open `http://localhost:8080` in your browser for a welcome message. Your backend is up and running.

### Rest API endpoints

- POST a book - `http://localhost:8080/api/public/book`
- POST an antique book - `http://localhost:8080/api/public/antique-book`
- POST a science journal - `http://localhost:8080/api/public/science-journal`
- Retrieve any type of book, by providing valid barcode - `http://localhost:8080/api/public/books/{barcode}`
- Calculate the total price of specific books - `http://localhost:8080/api/public/books/{barcode}/amount`
- Update (PUT) a book - `http://localhost:8080/api/public/book/{barcode}`
- Update (PUT) an antique book - `http://localhost:8080/api/public/antique-book/{barcode}`
- Update (PUT) a science journal - `http://localhost:8080/api/public/science-journal/{barcode}`
- Get all books (for testing) - `http://localhost:8080/api/public/books`

*Using live version, change url path `http://localhost:8080` to `https://dematic-books.herokuapp.com`*