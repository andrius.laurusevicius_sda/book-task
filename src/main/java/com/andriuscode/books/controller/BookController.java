package com.andriuscode.books.controller;

import com.andriuscode.books.model.AntiqueBook;
import com.andriuscode.books.model.Book;
import com.andriuscode.books.model.ScienceJournal;
import com.andriuscode.books.service.BookService;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.EAN;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/public/")
public class BookController {

    private final BookService bookService;

    @PostMapping("book")
    public ResponseEntity<String> saveBook(@Valid @RequestBody Book book) {
        bookService.addNewBook(book);
        return ResponseEntity.ok("Book added successfully");
    }

    @PostMapping("antique-book")
    public ResponseEntity<String> saveAntiqueBook(@Valid @RequestBody AntiqueBook antiqueBook) {
        bookService.addNewBook(antiqueBook);
        return ResponseEntity.ok("Antique book added successfully");
    }

    @PostMapping("science-journal")
    public ResponseEntity<String> saveScienceJournal(@Valid @RequestBody ScienceJournal scienceJournal) {
        bookService.addNewBook(scienceJournal);
        return ResponseEntity.ok("Science journal added successfully");
    }

    @GetMapping("books/{barcode}")
    public ResponseEntity<Book> findAnyBookByBarcode(@PathVariable("barcode")
                                                     @EAN(message = "It's not a valid EAN number") String barcode) {
        Book book = bookService.findAnyBookByBarcode(barcode);
        return ResponseEntity.ok(book);
    }

    @GetMapping("books/{barcode}/amount")
    public ResponseEntity<Map<String, BigDecimal>> getTotalPriceOfAnyBookType(@PathVariable("barcode")
                                                                              @EAN(message = "It's not a valid EAN number") String barcode) {
        Map<String, BigDecimal> response = new HashMap<>();
        response.put("totalPrice", bookService.getTotalPriceForAnyBook(barcode));
        return ResponseEntity.ok(response);
    }

    @PutMapping("book/{barcode}")
    public ResponseEntity<String> updateBook(@PathVariable("barcode")
                                             @EAN(message = "It's not a valid EAN number") String barcode,
                                             @Valid @RequestBody Book book) {
        bookService.updateAnyBook(book, barcode);
        return ResponseEntity.ok("Book updated successfully");
    }

    @PutMapping("antique-book/{barcode}")
    public ResponseEntity<String> updateAntiqueBook(@PathVariable("barcode")
                                                    @EAN(message = "It's not a valid EAN number") String barcode,
                                                    @Valid @RequestBody AntiqueBook antiqueBook) {
        bookService.updateAnyBook(antiqueBook, barcode);
        return ResponseEntity.ok("Antique book updated successfully");
    }

    @PutMapping("science-journal/{barcode}")
    public ResponseEntity<String> updateScienceJournal(@PathVariable("barcode")
                                                       @EAN(message = "It's not a valid EAN number") String barcode,
                                                       @Valid @RequestBody ScienceJournal scienceJournal) {
        bookService.updateAnyBook(scienceJournal, barcode);
        return ResponseEntity.ok("Science journal updated successfully");
    }

    @GetMapping("books")
    public ResponseEntity<List<Book>> getAllBooks() {
        return ResponseEntity.ok(bookService.getAllBooks());
    }
}
