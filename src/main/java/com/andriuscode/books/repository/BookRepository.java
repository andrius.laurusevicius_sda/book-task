package com.andriuscode.books.repository;

import com.andriuscode.books.model.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepository {

    public final List<Book> books;

    public BookRepository() {
        this.books = new ArrayList<>();
    }
}
