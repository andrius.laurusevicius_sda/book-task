package com.andriuscode.books.service;

import com.andriuscode.books.model.Book;
import com.andriuscode.books.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookService {

    private final BookRepository bookRepository;

    public void addNewBook(Book newBook) {
        checkIfBookExistByBarcode(newBook.getBarcode());
        log.info("Saving new book {}", newBook);
        bookRepository.books.add(newBook);
    }

    private void checkIfBookExistByBarcode(String barcode) {
        log.info("Searching book by barcode {}", barcode);
        bookRepository.books.stream()
                .filter(book -> book.getBarcode().equals(barcode)).findAny().ifPresent(book -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "Book with this EAN code already exist.");
                }
        );
    }

    public Book findAnyBookByBarcode(String barcode) {
        log.info("Retrieving book by barcode {}", barcode);
        return bookRepository.books.stream()
                .filter(book -> book.getBarcode().equals(barcode))
                .findAny().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Book with barcode %s not found", barcode)));
    }

    public BigDecimal getTotalPriceForAnyBook(String barcode) {
        return findAnyBookByBarcode(barcode).getPrice();
    }

    public void updateAnyBook(Book book, String barcode) {
        Book existingBook = findAnyBookByBarcode(barcode);
        int indexInList = bookRepository.books.indexOf(existingBook);

        checkIfBarcodesMatch(barcode, book.getBarcode());
        checkIfClassesMatch(book.getClass(), existingBook.getClass());

        log.info("Updating book {} with new values {}", existingBook, book);
        bookRepository.books.set(indexInList, book);
    }

    private void checkIfBarcodesMatch(String barcodeOne, String barcodeTwo) {
        if (!barcodeOne.equals(barcodeTwo)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Can't update book. Provided barcode %s must match barcode in body %s",
                        barcodeOne, barcodeTwo));
    }

    private void checkIfClassesMatch(Class<?> firstClass, Class<?> secondClass) {
        if (!firstClass.equals(secondClass)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Can't update book. Provided class %s doesn't match class in database %s.",
                        firstClass.getSimpleName(), secondClass.getSimpleName()));
    }

    public List<Book> getAllBooks() {
        log.info("Retrieving all books");
        return bookRepository.books;
    }
}
