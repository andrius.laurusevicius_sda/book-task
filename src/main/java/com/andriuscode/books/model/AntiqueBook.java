package com.andriuscode.books.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class AntiqueBook extends Book {

    @Max(value = 1900, message = "Antique books are with publishing date until 1900")
    @Positive(message = "Year value must be positive")
    @NotNull(message = "You must provide a release year value")
    private Integer releaseYear;

    @Override
    public BigDecimal getPrice() {
        return super.getPrice().multiply(BigDecimal.valueOf((LocalDate.now().getYear() - releaseYear) / 10));
    }
}
