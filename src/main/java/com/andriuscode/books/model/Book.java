package com.andriuscode.books.model;

import lombok.Data;
import org.hibernate.validator.constraints.EAN;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class Book {

    @NotBlank(message = "Book title can't be empty")
    private String name;

    @NotBlank(message = "Author name can't be empty")
    private String author;

    @NotBlank(message = "EAN code can't be empty")
    @EAN(message = "It's not a valid EAN number")
    private String barcode;

    @NotNull(message = "Quantity value can't be empty")
    @Min(value = 0, message = "The quantity can not be negative")
    private Integer quantity;

    @NotNull(message = "Price value can't be empty")
    @Digits(integer = 7, fraction = 2, message = "Price must have 2 decimal places")
    @Min(value = 0, message = "The price can't be negative")
    private BigDecimal pricePerUnit;

    public BigDecimal getPrice() {
        return pricePerUnit.multiply(BigDecimal.valueOf(quantity));
    }
}
