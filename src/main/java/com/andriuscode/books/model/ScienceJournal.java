package com.andriuscode.books.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
public class ScienceJournal extends Book {

    @Min(value = 1, message = "Minimum science index value is 1")
    @Max(value = 10, message = "Maximum science index value is 10")
    @NotNull(message = "You must provide a scienceIndex value")
    private Integer scienceIndex;

    @Override
    public BigDecimal getPrice() {
        return super.getPrice().multiply(BigDecimal.valueOf(scienceIndex));
    }
}
